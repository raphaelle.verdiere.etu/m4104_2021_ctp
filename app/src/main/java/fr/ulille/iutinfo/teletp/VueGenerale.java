package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle;
    private String poste;
    private String[] listSalle;
    private String DISTANCIEL ;
    // TODO Q2.c
    private SuiviViewModel sv;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment

        update();
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        listSalle=getContext().getResources().getStringArray(R.array.list_salles);
        DISTANCIEL= listSalle[0];
        // TODO Q2.c
        sv = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        // TODO Q4
        Spinner sp=(Spinner) getActivity().findViewById(R.id.spPoste) ;
        ArrayAdapter<CharSequence> adapterP = ArrayAdapter.createFromResource(super.getContext(), R.array.list_postes, android.R.layout.simple_spinner_item);
        adapterP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(adapterP);


        Spinner spSalle =(Spinner) getActivity().findViewById(R.id.spSalle) ;
        ArrayAdapter<CharSequence> adaptersalle = ArrayAdapter.createFromResource(super.getContext(), R.array.list_salles, android.R.layout.simple_spinner_item);
        adaptersalle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adaptersalle);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            EditText login=(EditText) getActivity().findViewById(R.id.tvLogin);
            sv.setUsername(login.getText().toString());

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Object item = parent.getItemAtPosition(pos);
                poste=item.toString();
                update();
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Object item = parent.getItemAtPosition(pos);
                salle=item.toString();
                update();
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        // TODO Q9
    }

    // TODO Q5.a
    private void update(){
        Spinner sp = (Spinner) getView().findViewById(R.id.spPoste);
        Spinner spSalle =(Spinner) getView().findViewById(R.id.spSalle);
        if (this.salle.equals("Distanciel")){
            sp.setVisibility(View.INVISIBLE);
            sp.setEnabled(false);
            sv.setLocalisation("Distanciel");
        }else {
            sp.setVisibility(View.VISIBLE);
            sp.setEnabled(true);
            sv.setLocalisation(this.salle+ " : " + this.poste);
        }


    }
    // TODO Q9
}