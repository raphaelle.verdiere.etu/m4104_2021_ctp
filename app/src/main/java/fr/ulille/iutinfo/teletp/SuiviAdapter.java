package fr.ulille.iutinfo.teletp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder> /* TODO Q6.a */ {
    // TODO Q6.a
    private SuiviViewModel model;
    private LayoutInflater mInflater;

    public SuiviAdapter(Context context, SuiviViewModel model){
        this.model=model;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String question= model.getQuestions(position);
        holder.myTextView.setText(question);
    }

    @Override
    public int getItemCount() {
        return model.questions.length;}

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView myTextView;ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.question);
        }
    }

    // TODO Q7
}